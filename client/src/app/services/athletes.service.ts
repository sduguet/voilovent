import { HttpClient } from '@angular/common/http';
import { isNgTemplate } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { Athlete } from '../models/athlete';

@Injectable({
  providedIn: 'root'
})
export class AthletesService {

  constructor(private httpClient: HttpClient) { }

  getAthletes(): Observable<any> {
    return this.httpClient.get('http://localhost:1337/api/athletes?populate=*').pipe(
      tap((response: any) => {
        return response;
      })
    )
  }

  getAthlete(id: String): Observable<any> {
    return this.httpClient.get('http://localhost:1337/api/athletes/' + id +'?populate=*').pipe(
      tap((response: any) => {

        response['customer'] = {
          firstName: response.data.attributes.customer.data.attributes.firstName,
          lastName: response.data.attributes.customer.data.attributes.lastName,
          photo: response.data.attributes.customer.data.attributes.photo,
          id: response.data.attributes.customer.data.id
        }
        response['isPro'] = response.data.attributes.isPro
        // response['ranking'] = response.data.attributes.ranking.data

        return response;
      })
    )
  }

}
