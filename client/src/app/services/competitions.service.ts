import { HttpClient } from '@angular/common/http';
import { isNgTemplate } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { RankingService } from 'src/app/services/rankings.service';
import { Competition } from '../models/competition';


@Injectable({
  providedIn: 'root'
})
export class CompetitionsService {

  constructor(private httpClient: HttpClient, private rankingService: RankingService) { }

  getCompetitions(): Observable<any> {
    return this.httpClient.get<any>('http://localhost:1337/api/competitions?populate=*').pipe(
        tap((response) => {
          response.data.map((item: any) => {
            Object.keys(item.attributes).forEach((attribute) => {
              item[attribute] = item.attributes[attribute]
            })
            if(item.attributes.type.data) {
              item['type'] = item.attributes.type.data.attributes
            }
            delete item['attributes']
          });
          return response.data;
        })
      )
  }

  getCompetition(id: String): Observable<any> {
    return this.httpClient.get<any>('http://localhost:1337/api/competitions/' + id + '?populate=*').pipe(
      tap((response) => {
        response['name'] = response.data.attributes.name
        response['place'] = response.data.attributes.place
        response['isPro'] = response.data.attributes.isPro
        response['dateStart'] = response.data.attributes.dateStart
        response['dateEnd'] = response.data.attributes.dateEnd
        response['type'] = response.data.attributes.type.data.attributes
        response['rankings'] = []
        response.data.attributes.rankings.data.forEach(async (ranking: any) => {
          await new Promise((resolve, reject) => {
            this.rankingService.getRanking(ranking.id).subscribe(ranking => {
              response['rankings'].push(ranking)
              resolve(response)
            }, err => reject(err));
          });
        });

        return response;
      })
    )
  }

  getCompetitionForRanking(id: String): Observable<any> {
    return this.httpClient.get<any>('http://localhost:1337/api/competitions/' + id + '?populate[rankings][populate][athlete]=*').pipe(
      tap((response) => {
        response['name'] = response.data.attributes.name
        response['place'] = response.data.attributes.place
        response['isPro'] = response.data.attributes.isPro
        response['dateStart'] = response.data.attributes.dateStart
        response['dateEnd'] = response.data.attributes.dateEnd
        response['rankings'] = response.data.attributes.rankings
        
        
        return response;
      })
    )
  }

  addCompetition(competition: Competition) {
    return this.httpClient.post('http://localhost:1337/api/competitions', { data :competition})
  } 
}
