import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { CompetitionsComponent } from './shared/competitions/competitions.component';

import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { PodiumComponent } from './shared/podium/podium.component';
import { ReturnPreviousComponent } from './shared/return-previous/return-previous.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AdminComponent } from './pages/admin/admin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { CompetitionsPageComponent } from './pages/competitions-page/competitions-page.component';
import { CompetitionsPastComponent } from './shared/competitions-past/competitions-past.component';
import { CompetitionsPastPageComponent } from './pages/competitions-past-page/competitions-past-page.component';

import { CustomerComponent } from './pages/customer/customer.component';
import { CompetitionComponent } from './pages/competition/competition.component';
import { RaceComponent } from './shared/race/race.component';
import { LoginComponent } from './shared/login/login.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { RegisterComponent } from './pages/register/register.component';
import { PlanComponent } from './pages/plan/plan.component';
import { MapComponent } from './map/map.component';

import { MarkerService } from './marker.service';
import { PopUpService } from './popup.service';

@NgModule({
  declarations: [
    AppComponent,
    CustomerComponent,
    CompetitionsComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    CompetitionComponent,
    PodiumComponent,
    ReturnPreviousComponent,
    DashboardComponent,
    RaceComponent,
    AdminComponent,
    LoginComponent,
    CompetitionsPageComponent,
    CompetitionsPastComponent,
    CompetitionsPastPageComponent,
    LoginPageComponent,
    LoginComponent,
    RegisterComponent,
    PlanComponent,
    MapComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    HttpClient,
    CookieService,
    DatePipe,
    MarkerService,
    PopUpService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
