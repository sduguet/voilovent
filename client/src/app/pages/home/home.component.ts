import { Component, OnInit } from '@angular/core';
import { Competition } from 'src/app/models/competition';
import { Ranking } from 'src/app/models/ranking';
import { CompetitionsService } from 'src/app/services/competitions.service';
import { RankingService } from 'src/app/services/rankings.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private competitionsService: CompetitionsService,
    private rankingService: RankingService,
  ) { }

  ngOnInit(): void {
    this.getCompetitions();
  }

  podium: Ranking[] = [];

  rankings: Ranking[] = [];

  getCompetitions() {
    this.competitionsService.getCompetitions().subscribe(response => {
      let nextCompet: any = [];
      response.data.forEach((competition: any) => {
        let date1 = new Date();
        let date2 = new Date(competition.dateStart);
        if (date1 > date2) {
          nextCompet.push(competition)
        }
      });
      nextCompet = nextCompet.sort((a: Competition, b: Competition) => {return (a.dateStart < b.dateStart) ? -1 : 1;}).splice(1)[0];
      console.log(nextCompet.rankings.data);
      setTimeout(() => {
        nextCompet.rankings.data.forEach((element : any) => {
          this.rankingService.getRanking(element.id).subscribe(response => {
            this.rankings.push(response);
          })
        })
      }, 100)
      setTimeout(() => {
        this.rankings = this.rankings.sort((a: Ranking, b: Ranking) => {
          return (a.rank < b.rank) ? -1 : 1;
        });
        this.podium = this.rankings.splice(0,3);
      }, 300)
    });
  }
}
