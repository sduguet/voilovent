import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Athlete } from 'src/app/models/athlete';
import { Competition } from 'src/app/models/competition';
import { Customer } from 'src/app/models/customer';
import { Ranking } from 'src/app/models/ranking';
import { CompetitionsService } from 'src/app/services/competitions.service';
import { CustomerService } from 'src/app/services/customer.service';
import { RankingService } from 'src/app/services/rankings.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  customer : Customer = {
    id: 0,
    firstName: '',
    lastName: '',
    photo: '',
  };

  currentCustomer : any = this.cookieService.get('userID');

  athlete: Athlete = {
    isPro: false,
    boat: '',
  };

  podium: Ranking[] = [];

  rankings: Ranking[] = [];

  constructor(
    private cookieService: CookieService,
    private customerService: CustomerService,
    private competitionsService: CompetitionsService,
    private rankingService: RankingService,
  ) { }

  ngOnInit(): void {
    this.getCustomer();
    this.getCompetitions();
  }

  getCustomer() {
    if(this.currentCustomer > 0) {
      this.customerService.getCustomer(this.currentCustomer).subscribe(response => {
        this.customer.firstName = response.data[0].attributes.firstName;
        this.customer.lastName = response.data[0].attributes.lastName;
        this.customer.photo = response.data[0].attributes.photo;
        this.athlete.isPro = response.data[0].attributes.athlete.data.attributes.isPro;
        this.athlete.boat = response.data[0].attributes.athlete.data.attributes.boat;
        this.athlete.customer = this.customer;
        this.customer.athlete = this.athlete;
      });
    }
  }

  getCompetitions() {
    this.competitionsService.getCompetitions().subscribe(response => {
      let nextCompet: any = [];
      response.data.forEach((competition: any) => {
        let date1 = new Date();
        let date2 = new Date(competition.dateStart);
        if (date1 > date2) {
          nextCompet.push(competition)
        }
      });
      nextCompet = nextCompet.sort((a: Competition, b: Competition) => {return (a.dateStart < b.dateStart) ? -1 : 1;}).splice(1)[0];
      console.log(nextCompet.rankings.data);
      setTimeout(() => {
        nextCompet.rankings.data.forEach((element : any) => {
          this.rankingService.getRanking(element.id).subscribe(response => {
            this.rankings.push(response);
          })
        })
      }, 100)
      setTimeout(() => {
        this.rankings = this.rankings.sort((a: Ranking, b: Ranking) => {
          return (a.rank < b.rank) ? -1 : 1;
        });
        this.podium = this.rankings.splice(0,3);
      }, 300)
    });
  }

}
