import { Component, Input, OnInit } from '@angular/core';
import { Competition } from 'src/app/models/competition';
import { CompetitionsService } from '../../services/competitions.service'
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Type } from '../../models/type'
import { TypesService } from 'src/app/services/types.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  @Input() competitions: Competition[] | any[] = [];

  form!: FormGroup;
  types: Array<Type>

  constructor(private fb: FormBuilder, private competitionsService: CompetitionsService, private typesService: TypesService) { }


  
  ngOnInit(): void {
    this.getCompetitions();
    this.getTypes();
    this.initForm();
  }

  

  initForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      place: ['', Validators.required],
      date: ['', Validators.required],
      type: ['', Validators.required],
      isPro: [false, Validators.required]
    })
  }

  submit() {
    console.log(this.form.value)
    this.competitionsService.addCompetition(this.form.value).subscribe(response => {
      console.log(response)
    })
  }

  getCompetitions() {
    this.competitionsService.getCompetitions().subscribe(response => {
      const nextCompet: any[] = [];
      response.data.forEach((competition: any) => {
        let date1 = new Date();
        let date2 = new Date(competition.dateStart);
        competition.past = date1 < date2;
        nextCompet.push(competition)
        competition.dateStart
      });
      console.log(nextCompet);
      
      this.competitions = nextCompet
    })
  }

  getTypes() {
    this.typesService.getTypes().subscribe(response => {
      this.types = response.data;
    })
  }

  

}
