import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup | any;
  loading = false;
  submitted = false;
  returnUrl: string;
  user:any = [];
  private cookieValue: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private httpClient: HttpClient,
    private cookieService: CookieService,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login(email:string, password:string):Observable<any> {
    return this.httpClient.post('http://localhost:1337/api/auth/local', {
      identifier: email,
      password: password,
    });;
  }

  setCookie(userID: number){
    if(userID !== 0) {
      this.cookieService.set('userID', userID.toString());
      this.cookieValue = this.cookieService.get('userID')
      location.href = "http://localhost:4200/dashboard"
    }
  }

  onDisconect(){
    this.cookieService.delete('userID');
  }

  check(){
    console.log(this.cookieService.get('userID'))
  }

  onSubmit() {
    this.submitted = true;

    if (this.form.invalid) {
        return;
    }
    
    this.login(this.form.value.username, this.form.value.password).subscribe(
      res => this.setCookie(res.user.id),
      err => console.log('HTTP Error', err),
    )
  }
}
