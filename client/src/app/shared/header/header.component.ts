import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Athlete } from 'src/app/models/athlete';
import { Customer } from 'src/app/models/customer';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  customer : Customer = {
    id: 0,
    firstName: '',
    lastName: '',
    photo: '',
  };
  
  currentCustomer : any = this.cookieService.get('userID');

  athlete: Athlete = {
    isPro: false,
    boat: '',
  };

  constructor(
    private cookieService: CookieService,
    private customerService: CustomerService,
  ) { }

  ngOnInit(): void {
    this.getCustomer();
  }

  getCustomer() {
    if(this.currentCustomer > 0) {
      this.customerService.getCustomer(this.currentCustomer).subscribe(response => {
        this.customer.firstName = response.data[0].attributes.firstName;
        this.customer.lastName = response.data[0].attributes.lastName;
        this.customer.photo = response.data[0].attributes.photo;
        this.athlete.isPro = response.data[0].attributes.athlete.data.attributes.isPro;
        this.athlete.boat = response.data[0].attributes.athlete.data.attributes.boat;
        this.athlete.customer = this.customer;
        this.customer.athlete = this.athlete;
      });
    }
  }

  onDisconect(){
    console.log("couou");
    this.cookieService.delete('userID');
    location.href = "http://localhost:4200/";
  }

}
