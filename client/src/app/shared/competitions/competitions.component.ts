import { Component, OnInit, Input } from '@angular/core';
import { Competition } from '../../models/competition'
import { CompetitionsService } from '../../services/competitions.service'

@Component({
  selector: 'app-competitions',
  templateUrl: './competitions.component.html',
  styleUrls: ['./competitions.component.scss']
})
export class CompetitionsComponent implements OnInit {
  
  title = 'Compétitions';
  @Input() limit = true;
  @Input() competitions: Competition[] = [];


  constructor(private competitionsService: CompetitionsService) { }

  ngOnInit(): void {
    this.getCompetitions();
  }

  getCompetitions() {
    this.competitionsService.getCompetitions().subscribe(response => {
      const nextCompet: any[] = [];
      if(!this.limit) {
        response.data.forEach((competition: any) => {
          let date1 = new Date();
          let date2 = new Date(competition.dateStart);
          if (date1 < date2) {
            nextCompet.push(competition)
          }
        });
        
        this.competitions = nextCompet.slice(0,3)
      } else {
        response.data.forEach((competition: any) => {
          let date1 = new Date();
          let date2 = new Date(competition.dateStart);
          if (date1 < date2) {
            nextCompet.push(competition)
          }
        });
        this.competitions = nextCompet
      }
    })
  }

}
