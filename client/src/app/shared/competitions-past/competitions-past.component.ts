import { Component, Input, OnInit } from '@angular/core';
import { Competition } from 'src/app/models/competition';
import { CompetitionsService } from 'src/app/services/competitions.service';

@Component({
  selector: 'app-competitions-past',
  templateUrl: './competitions-past.component.html',
  styleUrls: ['./competitions-past.component.scss']
})
export class CompetitionsPastComponent implements OnInit {

  title = 'Compétitions';
  @Input() limit = true;
  @Input() competitions: Competition[] = [];


  constructor(private competitionsService: CompetitionsService) { }

  ngOnInit(): void {
    this.getCompetitions();
  }

  getCompetitions() {
    this.competitionsService.getCompetitions().subscribe(response => {
      const nextCompet: any[] = [];
      if(!this.limit) {
        response.data.forEach((competition: any) => {
          let date1 = new Date();
          let date2 = new Date(competition.dateStart);
          if (date1 > date2) {
            nextCompet.push(competition)
          }
        });
        
        this.competitions = nextCompet.slice(0,3)
      } else {
        response.data.forEach((competition: any) => {
          let date1 = new Date();
          let date2 = new Date(competition.dateStart);
          if (date1 > date2) {
            nextCompet.push(competition)
          }
        });
        this.competitions = nextCompet
      }
    })
  }

}
