import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitionsPastComponent } from './competitions-past.component';

describe('CompetitionsPastComponent', () => {
  let component: CompetitionsPastComponent;
  let fixture: ComponentFixture<CompetitionsPastComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompetitionsPastComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitionsPastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
