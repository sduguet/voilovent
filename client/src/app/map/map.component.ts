import { Component, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import { MarkerService } from '../marker.service';
import { CompetitionsService } from '../services/competitions.service';

const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit {
  private map: L.Map | L.LayerGroup<any>;
  competitions: any[];

  constructor(private competitionsService: CompetitionsService, private markerService: MarkerService) { }
  

  private initMap(): void {
    this.map = L.map('map', {
      center: [46.599008, 1.878213],
      zoom: 6
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }

  ngAfterViewInit(): void {
    this.getCompetitions();
  }

  getCompetitions() {
    
    this.competitionsService.getCompetitions().subscribe((response: { data: any[]; }) => {
      const nextCompet: any[] = [];
        response.data.forEach((competition: any) => {
          let date1 = new Date();
          let date2 = new Date(competition.dateStart);
          if (date1 < date2) {
            nextCompet.push(competition)
          }
        });
        this.competitions = nextCompet
        this.createMarker()
      }
    )
    
  }

  createMarker() {
    this.initMap();
    this.markerService.makeCapitalMarkers(this.map, this.competitions);
  }
}