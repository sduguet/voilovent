import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopUpService {
  constructor() { }

  makeCapitalPopup(data: String): string {
    return `` +
      `<a href="/competition/${data}">Accéder à la compétition</div>`
  }
}