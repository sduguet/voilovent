import { Athlete } from "./athlete";
import { User } from "./user";

export interface Customer {
  id: Number,
  firstName: String,
  lastName: String,
  photo: String,
  athlete?: Athlete,
  user?: Number,
}
