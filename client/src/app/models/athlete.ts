import { Customer } from "./customer";
import { Ranking } from "./ranking";

export interface Athlete {
  id?: Number,
  isPro: Boolean,
  ranking?: Ranking,
  customer?: Customer,
  boat: String,
}
